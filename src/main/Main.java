package main;

import model.AgentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {

        ArrayList<AgentModel> data = prepareData();

        List<AgentModel> finalData = getTotalCollectionOfEachAgent(getGroupedDataOfList(data));

        for (AgentModel agent : finalData) {
            System.out.println(agent.toString());
        }

    }

    private static Map<Integer, List<AgentModel>> getGroupedDataOfList(ArrayList<AgentModel> data) {
        return data.stream().collect(Collectors.groupingBy(AgentModel::getAgent_id));
    }

    private static List<AgentModel> getTotalCollectionOfEachAgent(Map<Integer, List<AgentModel>> groupedData) {
        List<AgentModel> agentModelArrayList = new ArrayList<>();
        for (int agent_id : groupedData.keySet()) {
            int totalCollection = 0;
            for (AgentModel agentModel : groupedData.get(agent_id)) {
                totalCollection += Integer.parseInt(agentModel.getCollection());
            }
            agentModelArrayList.add(new AgentModel(agent_id, String.valueOf(totalCollection)));
        }
        return agentModelArrayList;
    }

    private static ArrayList<AgentModel> prepareData() {
        ArrayList<AgentModel> data = new ArrayList<>();

        data.add(new AgentModel(1, "120"));

        data.add(new AgentModel(2, "150"));

        data.add(new AgentModel(1, "130"));

        data.add(new AgentModel(2, "110"));

        data.add(new AgentModel(3, "100"));

        data.add(new AgentModel(2, "180"));

        data.add(new AgentModel(3, "160"));

        data.add(new AgentModel(1, "140"));

        data.add(new AgentModel(2, "190"));

        return data;
    }

}
