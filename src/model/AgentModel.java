package model;

public class AgentModel {

    private int agent_id;
    private String collection;

    public AgentModel() {
    }

    public AgentModel(int agent_id, String collection) {
        this.agent_id = agent_id;
        this.collection = collection;
    }

    public int getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(int agent_id) {
        this.agent_id = agent_id;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    @Override
    public String toString() {
        return "AgentModel{" +
                "agent_id=" + agent_id +
                ", collection='" + collection + '\'' +
                '}';
    }

    public AgentModel totalCollectionAmt(AgentModel agentModel) {
        int totalCollection = 0;
        totalCollection = +Integer.parseInt(agentModel.getCollection());
        this.setCollection(String.valueOf(totalCollection));
        return this;
    }
}
